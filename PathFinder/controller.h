//----------------------------------------------------------
//
//
//   	PathFinder Plugin
//                  by Pamdex
//
//
//	   
//----------------------------------------------------------
class Controller
{
public:
	Controller(CMapAndreas *mapAndreas);
	~Controller();
	void StartNewThread();

	//Public Data -> Main
	std::queue<pathWorkerData*> *qPath;
	std::queue<callbackWorkerData*> *qCallback;
	Mutex *workQueue;
	Mutex *callbackQueue;
	Mutex *mapAndreasQueue;
private:
	std::vector<Thread*> *threadList;
	CMapAndreas *mapAndreas;
};