//----------------------------------------------------------
//
//
//   	PathFinder Plugin
//                  by Pamdex
//
//
//	   
//----------------------------------------------------------
//                         INCLUDES
//----------------------------------------------------------
#include "main.h"
#include "mutex.h"
#include "path.h"
//----------------------------------------------------------
//                         Class
//----------------------------------------------------------
Path::Path(CMapAndreas *mapAndreas, Mutex *mapAndreasQueue)
{
	this->mapAndreas = mapAndreas;
	this->mapAndreasQueue = mapAndreasQueue;
	
	//Checking mode
	if (this->mapAndreas->GetOperatingMode() == MAP_ANDREAS_MODE_FULL || this->mapAndreas->GetOperatingMode() == MAP_ANDREAS_MODE_NOBUFFER)
	{
		//Mode is OK
		this->mapData = new std::map <int, mapPoint*>;
		this->pathData = new std::deque <mapPoint*>;
		this->vectorData = new std::queue <mapPoint*>;
		return;
	}
	else
	{
		//Error
		printf_s("PathFinder bad MapAndreas MODE - USE FULL OR NOBUFFER\n");
		return;
	}
}

double Path::getDistance(int x0, int y0, int x1, int y1)
{
	return sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));
}

mapPoint* Path::getMapPoint(int x, int y, int parentId, int step)
{
	mapPoint *tempMapPoint = new mapPoint();
	tempMapPoint->x = x;
	tempMapPoint->y = y;
	
	//Lock
	this->mapAndreasQueue->Lock();

	//Get data
	tempMapPoint->z = mapAndreas->FindZ_For2DCoord((float) x, (float) y);

	//Unlock
	this->mapAndreasQueue->Unlock();

	if (tempMapPoint->z == 0.0f) tempMapPoint->z = NULL_POINT;
	tempMapPoint->parentId = parentId;
	tempMapPoint->step = step;
	return tempMapPoint;
}

float Path::getZDifference(int nodeId, float z)
{
	float tempZ = 0.0f;

	//Lock
	mapAndreasQueue->Lock();

	//Get data
	tempZ = mapAndreas->FindZ_For2DCoord(0, 0, nodeId);

	//Unlock
	mapAndreasQueue->Unlock();

	return abs(tempZ - z);
}

short Path::findWay(float startX, float startY, float endX, float endY, float zDifference, int stepSize, int stepLimit, int maxSteps)
{
	//Set state
	short pathState = PATH_CALCULATING;

	//Set end XY
	int tempEndX = (int)round(endX);
	int tempEndY = (int)round(endY);

	//Start and end points
	mapPoint *startMapPoint = getMapPoint((int)startX, (int)startY, XYToNode((int)startX,(int)startY), 0);
	mapPoint *endMapPoint;

	mapData->insert(std::make_pair(XYToNode(startMapPoint->x, startMapPoint->y), startMapPoint));
	vectorData->push(startMapPoint);

	//Loop
	while (vectorData->size() > 0)
	{
		//Temp data
		int tempX = vectorData->front()->x;
		int tempY = vectorData->front()->y;
		float tempZ = vectorData->front()->z;
		//int tempParentId = vectorData->front()->parentId;
		int tempParentId = XYToNode(tempX, tempY);
		int tempStep = vectorData->front()->step;

		//Check step limit!
		if (tempStep == stepLimit)
		{
			pathState = PATH_STEP_LIMITED;
			break;
		}

		//Check max steps
		if (tempStep == maxSteps)
		{
			pathState = PATH_NOT_FOUND;
			break;
		}

		//Vector moves - test version!
		int tempNode = 0;

		// Y + 1
		if ((tempY + stepSize) < 3000)
		{
			tempNode = XYToNode(tempX, tempY + stepSize);
			if (!mapData->count(tempNode))
			{
				//Check Z difference
				if (getZDifference(tempNode, tempZ) <= zDifference)
				{
					//Get point
					mapPoint *tempMapPoint = this->getMapPoint(tempX, tempY + stepSize, tempParentId, tempStep + 1);
					mapData->insert(std::make_pair(tempNode, tempMapPoint));
					if (this->getDistance(tempMapPoint->x, tempMapPoint->y, tempEndX, tempEndY) <= stepSize)
					{
						pathState = PATH_FOUND;
						endMapPoint = tempMapPoint;
						break;
					}
					else vectorData->push(tempMapPoint);
				}
			}
		}

		// X + 1
		if ((tempX + stepSize) < 3000)
		{
			tempNode = XYToNode(tempX + stepSize, tempY);
			if (!mapData->count(tempNode))
			{
				//Check Z difference
				if (getZDifference(tempNode, tempZ) <= zDifference)
				{
					//Get point
					mapPoint *tempMapPoint = this->getMapPoint(tempX + stepSize, tempY, tempParentId, tempStep + 1);
					mapData->insert(std::make_pair(tempNode, tempMapPoint));
					if (this->getDistance(tempMapPoint->x, tempMapPoint->y, tempEndX, tempEndY) <= stepSize)
					{
						pathState = PATH_FOUND;
						endMapPoint = tempMapPoint;
						break;
					}
					else vectorData->push(tempMapPoint);
				}
			}
		}

		// Y - 1
		if ((tempY - stepSize) > -3000)
		{
			tempNode = XYToNode(tempX, tempY - stepSize);
			if (!mapData->count(tempNode))
			{
				//Check Z difference
				if (getZDifference(tempNode, tempZ) <= zDifference)
				{
					//Get point
					mapPoint *tempMapPoint = this->getMapPoint(tempX, tempY - stepSize, tempParentId, tempStep + 1);
					mapData->insert(std::make_pair(tempNode, tempMapPoint));
					if (this->getDistance(tempMapPoint->x, tempMapPoint->y, tempEndX, tempEndY) <= stepSize)
					{
						pathState = PATH_FOUND;
						endMapPoint = tempMapPoint;
						break;
					}
					else vectorData->push(tempMapPoint);
				}
			}
		}
		
		// X - 1
		if ((tempX - stepSize) > -3000)
		{
			tempNode = XYToNode(tempX - stepSize, tempY);
			if (!mapData->count(tempNode))
			{
				//Check Z difference
				if (getZDifference(tempNode, tempZ) <= zDifference)
				{
					//Get point
					mapPoint *tempMapPoint = this->getMapPoint(tempX - stepSize, tempY, tempParentId, tempStep + 1);
					mapData->insert(std::make_pair(tempNode, tempMapPoint));
					if (this->getDistance(tempMapPoint->x, tempMapPoint->y, tempEndX, tempEndY) <= stepSize)
					{
						pathState = PATH_FOUND;
						endMapPoint = tempMapPoint;
						break;
					}
					else vectorData->push(tempMapPoint);
				}
			}
		}

		// X + 1 | Y + 1
		if ((tempX + stepSize) < 3000 && (tempY + stepSize) < 3000)
		{
			tempNode = XYToNode(tempX + stepSize, tempY + stepSize);
			if (!mapData->count(tempNode))
			{
				//Check Z difference
				if (getZDifference(tempNode, tempZ) <= zDifference)
				{
					//Get point
					mapPoint *tempMapPoint = this->getMapPoint(tempX + stepSize, tempY + stepSize, tempParentId, tempStep + 1);
					mapData->insert(std::make_pair(tempNode, tempMapPoint));
					if (this->getDistance(tempMapPoint->x, tempMapPoint->y, tempEndX, tempEndY) <= stepSize)
					{
						pathState = PATH_FOUND;
						endMapPoint = tempMapPoint;
						break;
					}
					else vectorData->push(tempMapPoint);
				}
			}
		}

		// X + 1 | Y - 1
		if ((tempX + stepSize) < 3000 && (tempY - stepSize) > -3000)
		{
			tempNode = XYToNode(tempX + stepSize, tempY - stepSize);
			if (!mapData->count(tempNode))
			{
				//Check Z difference
				if (getZDifference(tempNode, tempZ) <= zDifference)
				{
					//Get point
					mapPoint *tempMapPoint = this->getMapPoint(tempX + stepSize, tempY - stepSize, tempParentId, tempStep + 1);
					mapData->insert(std::make_pair(tempNode, tempMapPoint));
					if (this->getDistance(tempMapPoint->x, tempMapPoint->y, tempEndX, tempEndY) <= stepSize)
					{
						pathState = PATH_FOUND;
						endMapPoint = tempMapPoint;
						break;
					}
					else vectorData->push(tempMapPoint);
				}
			}
		}

		// X - 1 | Y - 1
		if ((tempX - stepSize) > -3000 && (tempY - stepSize) > -3000)
		{
			tempNode = XYToNode(tempX - stepSize, tempY - stepSize);
			if (!mapData->count(tempNode))
			{
				//Check Z difference
				if (getZDifference(tempNode, tempZ) <= zDifference)
				{
					//Get point
					mapPoint *tempMapPoint = this->getMapPoint(tempX - stepSize, tempY - stepSize, tempParentId, tempStep + 1);
					mapData->insert(std::make_pair(tempNode, tempMapPoint));
					if (this->getDistance(tempMapPoint->x, tempMapPoint->y, tempEndX, tempEndY) <= stepSize)
					{
						pathState = PATH_FOUND;
						endMapPoint = tempMapPoint;
						break;
					}
					else vectorData->push(tempMapPoint);
				}
			}
		}

		// X - 1 | Y + 1
		if ((tempX - stepSize) > -3000 && (tempY + stepSize) < 3000)
		{
			tempNode = XYToNode(tempX - stepSize, tempY + stepSize);
			if (!mapData->count(tempNode))
			{
				//Check Z difference
				if (getZDifference(tempNode, tempZ) <= zDifference)
				{
					//Get point
					mapPoint *tempMapPoint = this->getMapPoint(tempX - stepSize, tempY + stepSize, tempParentId, tempStep + 1);
					mapData->insert(std::make_pair(tempNode, tempMapPoint));
					if (this->getDistance(tempMapPoint->x, tempMapPoint->y, tempEndX, tempEndY) <= stepSize)
					{
						pathState = PATH_FOUND;
						endMapPoint = tempMapPoint;
						break;
					}
					else vectorData->push(tempMapPoint);
				}
			}
		}

		//Next :)
		vectorData->pop();
	}
	
	//Get results
	if (pathState == PATH_FOUND)
	{
		//Normal path creating
		mapPoint *tempMapPoint = endMapPoint;
		pathData->push_back(tempMapPoint);
		
		//Loop :3
		while (tempMapPoint->step != 0)
		{
			tempMapPoint = mapData->at(tempMapPoint->parentId);
			pathData->push_back(tempMapPoint);
		}

		return pathState;
	}
	else if (pathState == PATH_STEP_LIMITED)
	{
		//Find nearest map point
		mapPoint *tempMapPoint;
		double nearDistance = 10000000;
		for (std::map<int, mapPoint*>::iterator i = mapData->begin(); i != mapData->end(); ++i)
		{
			double tempDistance = getDistance(tempEndX, tempEndY, i->second->x, i->second->y);
			if (tempDistance < nearDistance)
			{
				nearDistance = tempDistance;
				tempMapPoint = i->second;
			}
		}

		//Normal path creating from nearest point
		pathData->push_back(tempMapPoint);
		while (tempMapPoint->step != 0)
		{
			tempMapPoint = mapData->at(tempMapPoint->parentId);
			pathData->push_back(tempMapPoint);
		}

		return pathState;
	}
	return PATH_NOT_FOUND;
}

void Path::clear()
{
	for (std::map<int, mapPoint*>::iterator i = mapData->begin(); i != mapData->end(); ++i)
	{
		//Clearing...
		mapPoint *tempMapPoint = i->second;
		delete tempMapPoint;
	}
	mapData->clear();
	
	delete pathData;
	pathData = new std::deque <mapPoint*>;
	
	delete vectorData;
	vectorData = new std::queue <mapPoint*>;

}