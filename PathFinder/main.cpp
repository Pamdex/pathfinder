//----------------------------------------------------------
//
//
//   	PathFinder Plugin
//                  by Pamdex
//
//
//	   
//----------------------------------------------------------
//                         INCLUDES
//----------------------------------------------------------
#include "main.h"
#include "mutex.h"
#include "path.h"
#include "data.h"
#include "thread.h"
#include "controller.h"
//----------------------------------------------------------
// SDK Includes
//----------------------------------------------------------
#include "SDK/amx/amx.h"
#include "SDK/plugincommon.h"

typedef void (*logprintf_t)(const char* format, ...);
extern void *pAMXFunctions;
static logprintf_t logprintf;
//----------------------------------------------------------
// Main Plugin Data
//----------------------------------------------------------
bool inited = false;
int ticked = 0;
int maxTicked = 10;
std::vector<AMX*> amxList;

Controller *pController;

int pathCallbackIndex = -1;
cell tickAmxAddress[3] = { NULL };
//----------------------------------------------------------
// Main Plugin Functions
//----------------------------------------------------------
PLUGIN_EXPORT unsigned int PLUGIN_CALL Supports() 
{
	return SUPPORTS_VERSION | SUPPORTS_AMX_NATIVES | SUPPORTS_PROCESS_TICK;
}

PLUGIN_EXPORT bool PLUGIN_CALL Load( void **ppData ) 
{
	pAMXFunctions = ppData[PLUGIN_DATA_AMX_EXPORTS];
	logprintf = (logprintf_t)ppData[PLUGIN_DATA_LOGPRINTF];

	//Message
	logprintf("=========================================");
	logprintf("PathFinder Plugin %s", PATHFINDER_VERSION);
	logprintf("         by Pamdex");
	logprintf("");
	logprintf("Using MapAndreas Plugin 1.2.1");
	logprintf("Waiting for Init...");
	logprintf("=========================================");
	return true;
}

PLUGIN_EXPORT void PLUGIN_CALL Unload()
{
	//Unload controller and other stuffs
	
	inited = false; //VERY IMPORTANT !!!

	delete pController;
	logprintf( "PathFinder Plugin %s Unloaded!", PATHFINDER_VERSION);
}

// native PathFinder_Init(mapAndreasAddress, threads);
cell AMX_NATIVE_CALL n_pathfinder_init(AMX* amx, cell* params)
{
	if(!inited)
	{
		if (params[1] == NULL) return 0;

		pController = new Controller(new CMapAndreas((CMapAndreas*)params[1]));

		//Start threads
		for(int i = 0; i < params[2]; i++)
		{
			logprintf("PathFinder Plugin -> Creating New Thread");
			pController->StartNewThread();
		}

		inited = true;
	}
	return 1;
}

// native PathFinder_FindWay(routeid, Float:startX, Float:startY, Float:endX, Float:endY, Float:zDifference = 0.5, stepSize = 1, stepLimit = -1, maxSteps = 2000);
cell AMX_NATIVE_CALL n_pathfinder_findway(AMX* amx, cell* params)
{
	if (!inited) return 0;
	else
	{
		//Create data
		pathWorkerData *tempPathWorker = new pathWorkerData();
		tempPathWorker->routeiD = params[1];
		tempPathWorker->startX = amx_ctof(params[2]);
		tempPathWorker->startY = amx_ctof(params[3]);
		tempPathWorker->endX = amx_ctof(params[4]);
		tempPathWorker->endY = amx_ctof(params[5]);
		tempPathWorker->difference = amx_ctof(params[6]);
		tempPathWorker->stepSize = params[7];
		tempPathWorker->stepLimit = params[8];
		tempPathWorker->maxSteps = params[9];

		//Lock
		pController->workQueue->Lock();

		//Send
		pController->qPath->push(tempPathWorker);

		//Unlock
		pController->workQueue->Unlock();
		return 1;
	}
}

// native PathFinder_SetTickRate(rate = 10);
cell AMX_NATIVE_CALL n_pathfinder_settickrate(AMX* amx, cell* params)
{
	ticked = 0;
	if(params[1] <= 0) maxTicked = 1;
	else maxTicked = params[1];
	return 1;
}

// native PathFinder_MapAndreasLock();
cell AMX_NATIVE_CALL n_pathfinder_mapandreaslock(AMX* amx, cell* params)
{
	pController->mapAndreasQueue->Lock();
	return 1;
}

// native PathFinder_MapAndreasUnlock();
cell AMX_NATIVE_CALL n_pathfinder_mapandreasunlock(AMX* amx, cell* params)
{
	pController->mapAndreasQueue->Unlock();
	return 1;
}

AMX_NATIVE_INFO pathFinderNatives[] =
{
	{ "PathFinder_Init",					n_pathfinder_init },
	{ "PathFinder_FindWay",					n_pathfinder_findway },
	{ "PathFinder_SetTickRate",				n_pathfinder_settickrate },
	{ "PathFinder_MapAndreasLock",			n_pathfinder_mapandreaslock },
	{ "PathFinder_MapAndreasUnlock",		n_pathfinder_mapandreasunlock },
	{ 0,                        0 }
};

PLUGIN_EXPORT int PLUGIN_CALL AmxLoad( AMX *amx ) 
{
	amxList.push_back(amx);
	return amx_Register(amx, pathFinderNatives, -1);
}


PLUGIN_EXPORT int PLUGIN_CALL AmxUnload( AMX *amx ) 
{
	for(std::vector<AMX*>::iterator i = amxList.begin(); i != amxList.end();++i)
	{
		if(*i == amx)
		{
			amxList.erase(i);
			break;
		}
	}
	return AMX_ERR_NONE;
}

PLUGIN_EXPORT void PLUGIN_CALL ProcessTick()
{
	if(ticked == maxTicked)
	{
		if (inited)
		{
			//Lock - creating data for callback is very fast but...
			pController->callbackQueue->Lock();

			//Send data to PAWN :)
			while (!pController->qCallback->empty())
			{
				callbackWorkerData *tempCallbackWorker = pController->qCallback->front();
				cell * rawDataZ;
				cell * rawDataY;
				cell * rawDataX;
				
				//Prepare data
				rawDataZ = new cell[tempCallbackWorker->nodeX->size() + 1];
				rawDataY = new cell[tempCallbackWorker->nodeX->size() + 1];
				rawDataX = new cell[tempCallbackWorker->nodeX->size() + 1];
				copy(tempCallbackWorker->nodeZ->begin(), tempCallbackWorker->nodeZ->end(), rawDataZ);
				copy(tempCallbackWorker->nodeY->begin(), tempCallbackWorker->nodeY->end(), rawDataY);
				copy(tempCallbackWorker->nodeX->begin(), tempCallbackWorker->nodeX->end(), rawDataX);

				//Send data
				for (std::vector<AMX *>::iterator a = amxList.begin(); a != amxList.end(); ++a)
				{
					if (!amx_FindPublic(*a, "OnPathCalculated", &pathCallbackIndex))
					{
						//nodesSize
						amx_Push(*a, (int)tempCallbackWorker->nodeX->size());

						//nodesX, nodesY, nodesZ
						amx_PushArray(*a, &tickAmxAddress[2], 0, rawDataZ, tempCallbackWorker->nodeX->size() + 1);
						amx_PushArray(*a, &tickAmxAddress[1], 0, rawDataY, tempCallbackWorker->nodeX->size() + 1);
						amx_PushArray(*a, &tickAmxAddress[0], 0, rawDataX, tempCallbackWorker->nodeX->size() + 1);

						//other data
						amx_Push(*a, tempCallbackWorker->success);
						amx_Push(*a, tempCallbackWorker->routeId);
						amx_Exec(*a, NULL, pathCallbackIndex);

						//free
						amx_Release(*a, tickAmxAddress[0]);
						amx_Release(*a, tickAmxAddress[1]);
						amx_Release(*a, tickAmxAddress[2]);
					}
				}
				delete rawDataX;
				delete rawDataY;
				delete rawDataZ;
				delete pController->qCallback->front()->nodeX;
				delete pController->qCallback->front()->nodeY;
				delete pController->qCallback->front()->nodeZ;
				delete tempCallbackWorker;
				pController->qCallback->pop(); //next :)
			}

			//Unlock
			pController->callbackQueue->Unlock();
		}
		//-------
		ticked = 0;
	}
	ticked++;
}