//----------------------------------------------------------
//
//
//   	PathFinder Plugin
//                  by Pamdex
//
//
//	   
//----------------------------------------------------------
struct pathWorkerData
{
	int routeiD;
	float startX;
	float startY;
	float endX;
	float endY;
	int stepLimit;
	int stepSize;
	float difference;
	int maxSteps;
};
struct callbackWorkerData
{
	int routeId;
	bool success;
	bool stepsLimited;
	std::vector <int> *nodeX;
	std::vector <int> *nodeY;
	std::vector <int> *nodeZ;
};