//----------------------------------------------------------
//
//
//   	PathFinder Plugin
//                  by Pamdex
//
//
//	   
//----------------------------------------------------------
#include "MapAndreas/MapAndreas.h"
#define XYToNode(X,Y) (((Y - 3000) * -1) * 6000) + (X + 3000)
#define PATH_CALCULATING 0
#define PATH_FOUND 1
#define PATH_STEP_LIMITED 2
#define PATH_NOT_FOUND 3
struct mapPoint
{
	int x;
	int y;
	float z;
	int parentId;
	int step;
};
class Path
{
public:
	Path(CMapAndreas *mapAndreas, Mutex *mapAndreasQueue);
	std::deque <mapPoint*> *pathData; //calculated path
	
	float getZ(float x, float y, int dataPos = -1);
	double getDistance(int x0, int y0, int x1, int y1);
	mapPoint* getMapPoint(int x, int y, int parentId, int step);
	short findWay(float startX, float startY, float endX, float endY, float zDifference, int stepSize = 1, int stepLimit = -1, int maxSteps = 2000);
	void clear();
private:
	CMapAndreas *mapAndreas;
	Mutex *mapAndreasQueue;
	float getZDifference(int nodeId, float z);
	std::map<int, mapPoint*> *mapData;
	std::queue <mapPoint*> *vectorData;
};