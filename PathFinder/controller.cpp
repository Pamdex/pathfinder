//----------------------------------------------------------
//
//
//   	PathFinder Plugin
//                  by Pamdex
//
//
//	   
//----------------------------------------------------------
//                         INCLUDES
//----------------------------------------------------------
#include "main.h"
#include "mutex.h"
#include "path.h"
#include "data.h"
#include "thread.h"
#include "controller.h"
//----------------------------------------------------------
//                         Class
//----------------------------------------------------------
Controller::Controller(CMapAndreas *mapAndreas)
{
	//MapAndreas
	this->mapAndreas = mapAndreas;
	
	//Thread
	threadList = new std::vector<Thread*>;

	//Worker Data
	qPath = new std::queue<pathWorkerData*>;
	qCallback = new std::queue<callbackWorkerData*>;

	//Init global mutexes
	workQueue = new Mutex();
	workQueue->Init();

	callbackQueue = new Mutex();
	callbackQueue->Init();

	mapAndreasQueue = new Mutex();
	mapAndreasQueue->Init();
}
Controller::~Controller()
{
	//Delete Threads
	for(std::vector<Thread*>::iterator i = threadList->begin(); i != threadList->end();)
	{
		delete (*i);
		i = threadList->erase(i);
	}
	
	//Delete other stuffs
	delete qPath;
	delete qCallback;
	delete workQueue;
	delete callbackQueue;
	delete mapAndreasQueue;
	//delete mapAndreas;
}
void Controller::StartNewThread()
{
	Thread *tempThread = new Thread(new Path(mapAndreas, mapAndreasQueue), qPath, qCallback, workQueue, callbackQueue);
	threadList->push_back(tempThread);
}