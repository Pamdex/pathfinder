//----------------------------------------------------------
//
//
//   	PathFinder Plugin
//                  by Pamdex
//
//
//	   
//----------------------------------------------------------
//                         INCLUDES
//----------------------------------------------------------
#include "main.h"
#include "mutex.h"
#include "path.h"
#include "data.h"
#include "thread.h"
#include "SDK/amx/amx.h"
//----------------------------------------------------------
// Thread
//----------------------------------------------------------
Thread::Thread(Path *pPath, std::queue<pathWorkerData*> *qPath, std::queue<callbackWorkerData*> *qCallback, Mutex *workQueue, Mutex *callbackQueue)
{
	//Get path class
	this->pPath = pPath;

	//Get queues
	this->qPath = qPath;
	this->qCallback = qCallback;

	//Get mutexes
	this->workQueue = workQueue;

	this->callbackQueue = callbackQueue;

	//State
	this->threadState = new Mutex();

	this->state = ThreadState(WORKING);

	//Start thread
	START_THREAD(&Thread::RunPathCalculator, this); //fucking shit - 1h searching problem with this (NULL -> this)
}

Thread::~Thread()
{
	this->KillThread();
}

//Alive
bool Thread::IsAlive()
{
	//Temp array
	ThreadState tempData;

	//Block data access
	this->threadState->Lock();

	//Get data
	tempData = this->state;

	//Unlock data access
	this->threadState->Unlock();

	//Return data
	if (tempData == ThreadState(WORKING)) return true;
	else return false;
}

//Stoped
bool Thread::IsStoped()
{
	//Temp array
	ThreadState tempData;

	//Block data access
	this->threadState->Lock();

	//Get data
	tempData = this->state;

	//Unlock data access
	this->threadState->Unlock();

	//Return data
	if (tempData == ThreadState(STOPED)) return true;
	else return false;
}

//Kill
void Thread::KillThread()
{
	//Block data access
	this->threadState->Lock();

	//Set data
	this->state = ThreadState(STOPING);

	//Unlock data access
	this->threadState->Unlock();

	//Wait for thread kill
	while(true)
	{
		if(this->IsStoped()) break;
	}
}

//Wrapper
#ifdef WIN32
void Thread::RunPathCalculator(void *obj)
#else
void *Thread::RunPathCalculator(void *obj)
#endif
{
	static_cast<Thread*>(obj)->PathCalculator(NULL);
	//return ((Thread *)obj)->PathCalculator(NULL); also working
}

//PathCalculator
#ifdef WIN32
void Thread::PathCalculator(void *unused)
#else
void *Thread::PathCalculator(void *unused)
#endif
{
	//Loop
	while(this->IsAlive())
	{
		workQueue->Lock();
		if(!qPath->empty())
		{
			//Get data
			pathWorkerData *tempPathWorker = qPath->front();
			int routeID = tempPathWorker->routeiD;
			float startX = tempPathWorker->startX;
			float startY = tempPathWorker->startY;
			float endX = tempPathWorker->endX;
			float endY = tempPathWorker->endY;
			float zDifference = tempPathWorker->difference;
			int stepSize = tempPathWorker->stepSize;
			int stepLimit = tempPathWorker->stepLimit;
			int maxSteps = tempPathWorker->maxSteps;
			qPath->pop();
			delete tempPathWorker;

			//Unlock
			workQueue->Unlock();

			//Find path - looks simply but...
			short state = pPath->findWay(startX, startY, endX, endY, zDifference, stepSize, stepLimit, maxSteps);

			//Send data - lock mutex
			callbackQueue->Lock();

			if (state != PATH_NOT_FOUND)
			{
				//Get data
				callbackWorkerData *tempCallbackWorker = new callbackWorkerData();
				tempCallbackWorker->routeId = routeID;
				tempCallbackWorker->success = true;
				if (stepLimit != -1) tempCallbackWorker->stepsLimited = true;
				else tempCallbackWorker->stepsLimited = false;

				//Init
				tempCallbackWorker->nodeX = new std::vector<int>;
				tempCallbackWorker->nodeY = new std::vector<int>;
				tempCallbackWorker->nodeZ = new std::vector<int>;

				//Get nodes
				while (!pPath->pathData->empty())
				{
					float tempX = (float)pPath->pathData->back()->x;
					float tempY = (float)pPath->pathData->back()->y;
					tempCallbackWorker->nodeX->push_back(amx_ftoc(tempX));
					tempCallbackWorker->nodeY->push_back(amx_ftoc(tempY));
					tempCallbackWorker->nodeZ->push_back(amx_ftoc(pPath->pathData->back()->z));
					pPath->pathData->pop_back();
				}

				qCallback->push(tempCallbackWorker);
			}
			else
			{
				//Create empty data ;C
				callbackWorkerData *tempCallbackWorker = new callbackWorkerData();
				tempCallbackWorker->routeId = routeID;
				tempCallbackWorker->success = false;
				if (stepLimit != -1) tempCallbackWorker->stepsLimited = true;
				else tempCallbackWorker->stepsLimited = false;
				
				//Init
				tempCallbackWorker->nodeX = new std::vector<int>;
				tempCallbackWorker->nodeY = new std::vector<int>;
				tempCallbackWorker->nodeZ = new std::vector<int>;
				
				tempCallbackWorker->nodeX->push_back(0);
				tempCallbackWorker->nodeY->push_back(0);
				tempCallbackWorker->nodeZ->push_back(0);

				qCallback->push(tempCallbackWorker);
			}

			//Unlock
			callbackQueue->Unlock();

			//Clear
			pPath->clear();
			SLEEP(5); //Wait ...
		}
		else //Waiting...
		{
			workQueue->Unlock();
			SLEEP(20);
		}
	}
	
	//Block data access
	this->threadState->Lock();

	//Set data
	this->state = ThreadState(STOPED);

	//Unlock data access
	this->threadState->Unlock();
	
	EXIT_THREAD();
}