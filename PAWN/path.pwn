#include <a_samp>
#include <MapAndreas> //1.2.1
#include <PathFinder>
#include <streamer>
#pragma dynamic 100000
#define PATHFINDER_THREADS 8

//MapAndreas Fix
stock MapAndreas_FindZ_For2DCoordEx(Float:x, Float:y, &Float:z) {

    if(x >= 3000.0) x = 2999.0;
    if(x <= -3000.0) x = -2999.0;

    if(y >= 3000.0) y = 2999.0;
    if(y <= -3000.0) y = -2999.0;

    MapAndreas_FindZ_For2DCoord(x,y,z);
}

#define MapAndreas_FindZ_For2DCoord MapAndreas_FindZ_For2DCoordEx
//----------------------------------------------------------------------
new Float:point[MAX_PLAYERS][3];
new text[128];
public OnPathCalculated(routeid, success, Float:nodesX[], Float:nodesY[], Float:nodesZ[], nodesSize)
{
	format(text, sizeof(text), "PATH: route: %d success: %d nodesSize: %d", routeid, success, nodesSize);
	SendClientMessageToAll(-1, text);
	if(success)
	{
		for(new i; i < nodesSize; i++)
	    {
			CreateDynamicObject(19130, nodesX[i], nodesY[i], nodesZ[i] + 1, 0, 0, 0);
    	}
    	Streamer_Update(0);
	}
	return 1;
}
public OnFilterScriptInit()
{
	MapAndreas_Init(MAP_ANDREAS_MODE_FULL);
	PathFinder_Init(MapAndreas_GetAddress(), PATHFINDER_THREADS);
	return 1;
}
public OnPlayerCommandText(playerid,cmdtext[])
{
	if(!strcmp("/path_test", cmdtext, true))
	{
		new Float:x, Float:y, Float:z;
		GetPlayerPos(playerid, x, y, z);
		PathFinder_FindWay(0, x, y, point[playerid][0], point[playerid][1], 0.9);
	    return 1;
	}
	if(!strcmp("/path_test_size", cmdtext, true))
	{
		new Float:x, Float:y, Float:z;
		GetPlayerPos(playerid, x, y, z);
		PathFinder_FindWay(0, x, y, point[playerid][0], point[playerid][1], 0.9, .stepSize = 2);
	    return 1;
	}
	if(!strcmp("/path_test_limit", cmdtext, true))
	{
		new Float:x, Float:y, Float:z;
		GetPlayerPos(playerid, x, y, z);
		PathFinder_FindWay(0, x, y, point[playerid][0], point[playerid][1], 0.9, .stepLimit = 30);
	    return 1;
	}
	if(!strcmp("/path_test_limit_50", cmdtext, true))
	{
		new Float:x, Float:y, Float:z;
		GetPlayerPos(playerid, x, y, z);
		for(new i = 0; i < 50; i++)
		{
		    PathFinder_FindWay(i, x, y, random(6000) - 3000, random(6000) - 3000, 0.9, .stepLimit = 10);
		}
		return 1;
	}
	if(!strcmp("/path_mapandreas", cmdtext, true))
	{
		new Float:z;
		PathFinder_MapAndreasLock(); //Block PathFinder...

		//Do MapAndreas command or smth else like RNPC (with MapAndreas class sharing)
		for(new i = 0; i < 1000; i++)
		{
		    MapAndreas_FindZ_For2DCoord(random(6000) - 3000, random(6000) - 3000, z);
		    format(text, sizeof(text), "%d -> %f", i, z);
		    SendClientMessageToAll(-1, text);
		}

		PathFinder_MapAndreasUnlock();
		return 1;
	}
	return 0;
}
public OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ)
{
	point[playerid][0] = fX;
	point[playerid][1] = fY;
	point[playerid][2] = fZ;
	format(text, sizeof(text), "%f %f %f", point[playerid][0], point[playerid][1], point[playerid][2]);
	SendClientMessageToAll(-1, text);
	return 1;
}