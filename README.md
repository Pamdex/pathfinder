# PathFinder SA:MP Plugin by Pamdex #

This plugin allows you to **calculate a route from point A to B** on the San Andreas map.

It uses the **built-in MapAndreas 1.2.1 class integration** and **Djikstra** styled algorithm, so you can calculate routes everywhere (not in water).

It also uses separate thread for path calculation.

**Dijkstra:**

![Algorithm](http://upload.wikimedia.org/wikipedia/commons/2/23/Dijkstras_progress_animation.gif)

[More info](http://forum.sa-mp.com/showthread.php?t=427227)